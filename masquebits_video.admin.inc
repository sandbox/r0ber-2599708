<?php

function msqvideo_form($form, &$form_state, $msqvideo = NULL) {

	$form = array();

	$form['name'] = array(
	'#title' => t('name'),
	'#type' => 'textfield',
	'#default_value' => isset($msqvideo->name) ? $msqvideo->name : '',
	'#description' => t('Video title'),
	'#required' => TRUE,
	'#weight' => -50,

	);

	$form['embedded'] = array(
	'#title' => t('embedcode'),
	'#type' => 'textfield',
	'#default_value' => isset($msqvideo->embedded) ? $msqvideo->embedded : '',
	'#description' => t('Video embedcode'),
	'#required' => TRUE,


	);

	$form['duration'] = array(
	'#title' => t('duration'),
	'#type' => 'textfield',
	'#default_value' => isset($msqvideo->duration) ? $msqvideo->duration : '',
	'#description' => t('Video duration'),
	'#required' => TRUE,


	);

        field_attach_form('msqvideo', $msqvideo, $form, $form_state);
        
	$form['actions'] = array(
		'#type' => 'actions',
		'submit' => array(
                  '#type' => 'submit',
                  '#value' => isset($msqvideo->id) ? t('Update video asset') : t('Save video asset'),
                    ),
		);

	return $form;


}

function msqvideo_form_submit($form, &$form_state) {

	$videoMsq = entity_ui_form_submit_build_entity($form, $form_state);
	dsm($videoMsq );
	$videoMsq->save();
	drupal_set_message(t('@name video has been saved.', array('@name' => $videoMsq->name)));
	$form_state['redirect'] = 'admin/msq_videoentity';

}